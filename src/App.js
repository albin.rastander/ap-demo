import React, { Component } from "react";
import "./App.css";
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import { HotkeyPlugin, DiscreteAudioPlugin, VttSubtitlePlugin } from "@accurate-player/accurate-player-plugins";
import "@accurate-player/accurate-player-controls";
import { TimeFormat, SeekMode } from "@accurate-player/accurate-player-core";

class App extends Component {
  videoElement;
  apControls;
  player;
  hotkeyPlugin;
  discreteAudioPlugin;
  vttSubtitlePlugin;

  componentDidMount() {
    this.player = new ProgressivePlayer(this.videoElement, window.LICENSE_KEY);
    // Load video file to player.
    this.player.api.loadVideoFile({
      label: "Sintel Timecode",
      src: `https://dylanfischler-public.s3-us-west-1.amazonaws.com/race_car.mp4`,
      enabled: false,
      frameRate: {
        numerator: 23.82,
        denominator: 1
      },
      dropFrame: false
    });
    // Initialize and register plugins to player.
    this.initPlugins();

    // Initialize UI-controls.
    this.initControls();

    // Register a discrete audio track.
    this.discreteAudioPlugin.initDiscreteAudioTracks(
      [
        {
          // Enabled determines if the track should be enabled at once.
          enabled: false,
          track: {
            // Label, visible in UI
            label: "Sintel - No Speak",
            // channel count of track
            channelCount: 2,
            // url of the file.
            src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/aac-discrete/sintel-no-speak.mp4"
          }
        },
        {
          // Add track with a 5 seconds offset, i.e src file is 5 seconds off.
          enabled: true,
          track: {
            label: "Sintel - offset",
            channelCount: 2,
            // set offset to compensate for file offset, should now play in sync with video.
            offset: -5,
            src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/aac-discrete/sintel_stereo_5s_offset.mp4"
          }
        }
      ]
    );

    // Register subtitle tracks
    // All registered plugins are available from players api as well.
    this.player.api.plugin.apVttSubtitlePlugin.createVttSubtitles(
      [
        {
          enabled: true,
          kind: "subtitles",
          label: "English",
          src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/sub/sintel_en.vtt",
          srclang: "en"
        },
        {
          kind: "subtitles",
          label: "Swedish",
          src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/sub/sintel_swe.vtt",
          srclang: "se"
        }
      ]
    );
  
    // Register custom hotkey to toggle visibility of all enabled tracks
  this.hotkeyPlugin.setHotkey(
    "shift+t",
    () => {
      var subtitles = this.vttSubtitlePlugin.vttSubtitles;
      subtitles.forEach(track => {
        this.vttSubtitlePlugin.toggleVttSubtitle(track);
      });
    },
    "Toggle all subtitle tracks"
  );

  this.videoElement.addEventListener('loadeddata', () => {
    this.player.api.seek({
     time: Number(300),
     mode: SeekMode.Absolute,
     format: TimeFormat.Frame,
    });
   }, false);
 
  }
  initPlugins() {
    this.hotkeyPlugin = new HotkeyPlugin(this.player);

    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);

    this.vttSubtitlePlugin = new VttSubtitlePlugin(this.player);
  }

  initControls() {
    // Check if method exist, otherwise wait 100ms and try again.
    if (!this.apControls.init) {
      setTimeout(() => {
        this.initControls();
      }, 100);
      return;
    }

    // The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
    this.apControls.init(this.videoElement, this.player, {
      saveLocalSettings: true // Enables storing of simple settings as volume, timeFormat and autoHide.
    });
  }

  render() {
    return (
      <div className="App">
        <div className="video-container">
          <video
            ref={ref => (this.videoElement = ref)}
            crossOrigin="true"
            playsInline={true}
          />
          <apc-controls ref={ref => (this.apControls = ref)} />
        </div>
        <div>
          <button onClick={() => this.player.api.play()}>Play</button>
          <button onClick={() => this.player.api.pause()}>Pause</button>
        </div>
      </div>
    );
  }
}

export default App;
