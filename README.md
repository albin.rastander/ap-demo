This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It demonstrates how to integrate Accurate Player within an React application and playing a statically hosted file using the `ProgressivePlayer`. The `HotkeyPlugin` as well as the UI layer `<apc-controls>` is activated.

## Setup

1. Login to Accurate Player npm repository to gain access to required dependencies, username and password will be provided to you by the Accurate Player team.
> npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player

2. Add your personal license key to the "license-key.js" file located in project root.

## Running the demo

1. `npm install`
2. `npm run start`
3. Open `http://localhost:8080` in your browser.
